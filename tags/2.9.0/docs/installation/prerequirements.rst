Prerequirements
===============

A recent enough version of the Apache HTTP server is required. 2.2.6 or later
should be used. In addition, the apr-util library needs to be 1.3.0 or newer.
This is because the DBD database pool functionality was developed mainly
between 2006 and 2007, and reached production quality at the time.
